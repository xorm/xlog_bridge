// Copyright 2021 The XORM Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package bridge

import (
	"strings"

	log "gitea.com/lunny/xlog"
	xorm_log "xorm.io/xorm/log"
)

var (
	formats           []string
	defaultFormatSize = 20
)

func genFormat(argsLen int) string {
	return strings.TrimSpace(strings.Repeat("%v ", argsLen))
}

func init() {
	formats = make([]string, defaultFormatSize, defaultFormatSize)
	for i := 0; i < defaultFormatSize; i++ {
		formats[i] = genFormat(i)
	}
}

// XLogBridge a logger bridge from Logger to xorm
type XLogBridge struct {
	showSQL bool
	logger  *log.Logger
}

// NewXLogger inits a log bridge for xorm
func NewXLogger(name string, showSQL bool) xorm_log.Logger {
	return &XLogBridge{
		showSQL: showSQL,
		logger:  log.GetLogger(name),
	}
}

// Debug show debug log
func (l *XLogBridge) Debug(v ...interface{}) {
	l.logger.Debug(strings.Repeat("%v ", len(v)), v...)
}

// Debugf show debug log
func (l *XLogBridge) Debugf(format string, v ...interface{}) {
	l.logger.Debug(format, v...)
}

// Error show error log
func (l *XLogBridge) Error(v ...interface{}) {
	l.logger.Error(strings.Repeat("%v ", len(v)), v...)
}

// Errorf show error log
func (l *XLogBridge) Errorf(format string, v ...interface{}) {
	l.logger.Error(format, v...)
}

// Info show information level log
func (l *XLogBridge) Info(v ...interface{}) {
	l.logger.Info(strings.Repeat("%v ", len(v)), v...)
}

// Infof show information level log
func (l *XLogBridge) Infof(format string, v ...interface{}) {
	l.logger.Info(format, v...)
}

// Warn show warning log
func (l *XLogBridge) Warn(v ...interface{}) {
	l.logger.Warn(strings.Repeat("%v ", len(v)), v...)
}

// Warnf show warnning log
func (l *XLogBridge) Warnf(format string, v ...interface{}) {
	l.logger.Warn(format, v...)
}

// Level get logger level
func (l *XLogBridge) Level() xorm_log.LogLevel {
	switch l.logger.GetLevel() {
	case "debug", "trace":
		return xorm_log.LOG_DEBUG
	case "info":
		return xorm_log.LOG_INFO
	case "warn":
		return xorm_log.LOG_WARNING
	case "error", "critical":
		return xorm_log.LOG_ERR
	}
	return xorm_log.LOG_OFF
}

// SetLevel set the logger level
func (l *XLogBridge) SetLevel(lvl xorm_log.LogLevel) {
}

// ShowSQL set if record SQL
func (l *XLogBridge) ShowSQL(show ...bool) {
	if len(show) > 0 {
		l.showSQL = show[0]
	} else {
		l.showSQL = true
	}
}

// IsShowSQL if record SQL
func (l *XLogBridge) IsShowSQL() bool {
	return l.showSQL
}
