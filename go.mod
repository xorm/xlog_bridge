module gitea.com/xorm/xlog_bridge

go 1.12

require (
	gitea.com/lunny/xlog v0.0.0-20210305075605-5b8499deada8
	xorm.io/xorm v1.0.7
)
